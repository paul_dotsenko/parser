<?php

namespace App\Console\Commands;

use Goutte\Client;
use Illuminate\Console\Command;
use Symfony\Component\DomCrawler\Crawler;

class TestParseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search {trademark?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse search engine results pages (SERPs) from "https://search.ipaustralia.gov.au/trademarks/search/advanced';

	/**
	 * Search Engine Url
	 *
	 * @var string
	 */
    public string $url = 'https://search.ipaustralia.gov.au/trademarks/search/advanced';

	/**
	 * Search page Crawler instance.
	 *
	 * @var Crawler
	 */
    public Crawler $crawler;

	/**
	 * Variable holding total result amount.
	 *
	 * @var int
	 */
    public int $total = 0;

	/**
	 * Search engine results data array.
	 *
	 * @var array
	 */
    public array $results = [];

	/**
	 * Variable to define time spent on operation.
	 *
	 * @var int
	 */
    public int $timeStart;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->timeStart = microtime(true);
    }

	/**
     * Execute the console command.
     */
    public function handle()
    {
    	$keyword = $this->argument('trademark') ?: $this->ask('Type Trademark to search');

    	if (!$keyword) {
    		$this->warn('No search Keyword defined. Search cancelled.');
    		exit;
	    }

    	$this->parseResults($keyword);
    	$this->renderResults();

    	$timeElapsed = round(microtime(true) - $this->timeStart, 3);
    	$this->output->writeln('*Time elapsed: ' . $this->styleAsInteger( '~' . strval($timeElapsed) ) . ' seconds.');
    	echo PHP_EOL;
    }

	/**
	 * Display results in terminal.
	 */
	public function renderResults()
	{
		$this->comment('Search results:' . PHP_EOL);
		$tab = "\t";

		echo '[' . PHP_EOL;

		foreach ($this->results as $i => $result) {

			echo $tab . $i . '. ' . '{' . PHP_EOL;

			foreach ($result as $key => $value) {

				echo $tab . $tab;
				$this->output->writeln(implode(': ', [$key, $value]));

			}

			echo $tab . '}' . (isset($this->results[$i + 1]) ? ',' : '');
			$this->output->newLine(2);
		}

		$this->output->writeln($tab . '<fg=yellow;options=bold>Total results:</> ' . $this->styleAsInteger( $this->total ));

		$this->output->writeln(']' . PHP_EOL);
	}

	protected function parseResults($keyword)
    {
    	$client = new Client();
    	$crawler = $client->request('GET', $this->url);
    	$form = $crawler->selectButton('Search')->form();
    	$this->crawler = $client->submit($form, ['wv[0]' => $keyword]);

    	$paginationCountElement = $this->crawler->filter('div.pagination-count');

    	if (!$paginationCountElement->count()) {
    		$this->warn('No results found.');
    		exit;
	    }

    	preg_match('/Results ([\d]+) to ([\d]+) of ([\d]+)/', $paginationCountElement->text(), $paginationCounts);

    	$this->total = $paginationCounts[3];

    	for ($page = 1; $this->total > count($this->results); $page++) {

    		$linksBar = $this->crawler->filter('div.pagination-buttons');
    		$linkObj = $linksBar->selectLink($page);
    		$link = $linkObj->link();
    		$this->crawler = $client->click($link);
    		$this->crawler
			    ->filter('table#resultsTable > tbody')
			    ->each(function (Crawler $row) {
			    	$this->fetchRowData($row);
		    });
	    }

    	print_r($this->results);
    }

    protected function fetchRowData(Crawler $row)
    {
    	$status = trim(preg_replace('/[^\w\s]/', '', $this->getDataIfNodeNotEmpty($row, 'td.status')));

    	$data = [
    		'number' => $this->styleAsInteger( $this->getDataIfNodeNotEmpty($row, 'td.number > a') ),
		    'logo_url' => $this->styleAsLink( $this->getDataIfNodeNotEmpty($row, 'td.image > img', 'src') ),
		    'name' => $this->styleAsString( $this->getDataIfNodeNotEmpty($row, 'td.words') ),
		    'classes' => preg_replace('/(\d)/', $this->styleAsInteger('\\1'), $this->getDataIfNodeNotEmpty($row, 'td.classes')),
		    'status1' => $this->styleAsString( $this->getStatusCut($status) ),
		    'status2' => $this->styleAsString( $status ),
		    'detailed_page_url' => $this->styleAsLink( $row->attr('data-markurl') ),
	    ];

    	empty($this->results)
		    ? $this->results[1] = $data
		    : $this->results[] = $data;

    }

    protected function getDataIfNodeNotEmpty(Crawler $node, $filterSelector = '', $attrName = '')
    {
    	if ($filterSelector) {
    		$node = $node->filter($filterSelector);
	    }

	    if (!$node->count()) return '';

	    $data = $attrName
		    ? $node->attr($attrName)
		    : $node->text();

	    return trim($data);
    }

    protected function getStatusCut(string $status)
    {
    	if ($status) {

		    if (strpos($status, '-') !== false) {

			    $res = strstr($status, '-', true);

		    } elseif (strpos($status, ':') !== false) {

			    $res = strstr($status, ':', true);

		    } else {

			    $res = $status;

		    }

		    return trim($res);
	    }

    	return '';
    }

    protected function styleAsInteger(string $value)
    {
    	return '<fg=blue;options=bold>' . $value . '</>';
    }

    protected function styleAsLink(string $value)
    {
    	return '<fg=yellow;options=underscore>' . $value . '</>';
    }

    protected function styleAsString(string  $value)
    {
	    return '<fg=green>' . $value . '</>';
    }
}
